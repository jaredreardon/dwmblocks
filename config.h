#define CMDLENGTH 50
#define DELIMITER " | "
#define TRAILING_DELIMITER
#define CLICKABLE_BLOCKS

const Block blocks[] = {
    BLOCK("cat /tmp/recordingicon 2>/dev/null",    0, 9)
	BLOCK("sb_cpu",    3, 14)
	BLOCK("sb_memory",   5,    13)
	BLOCK("sb_updates",  0, 8)
	BLOCK("sb_disk",  90, 12)
	BLOCK("sb_forecast",  3600, 3)
	BLOCK("sb_internet",  5, 4)
	BLOCK("sb_volume",  0,    10)
	BLOCK("sb_clock", 1,    1)
};
